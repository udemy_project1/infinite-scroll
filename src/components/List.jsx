import React from "react";
import { useState, useEffect, useRef } from "react";
import usePhotos from "../hooks/usePhotos";
import spinner from "../assets/spinner.svg";

export default function List() {
	const [query, setQuery] = useState("random");
	const [pagneNumber, setPageNumber] = useState(1);
	const lastPicRef = useRef();

	const photoApiData = usePhotos(query, pagneNumber);

	const searchRef = useRef();
	const handleSubmit = (e) => {
		e.preventDefault();
		if (searchRef.current.value !== query) {
			setQuery(searchRef.current.value);
			setPageNumber(1);
		}
	};
	useEffect(() => {
		if (lastPicRef.current) {
			const observer = new IntersectionObserver(([entry]) => {
				if (entry.isIntersecting && photoApiData.maxPages !== pagneNumber) {
					setPageNumber(pagneNumber + 1);
					lastPicRef.current = null;
					observer.disconnect();
				}
			});
			observer.observe(lastPicRef.current);
		}
	}, [photoApiData]);
	return (
		<>
			<h1 className="text-4xl ">Unsplash Clone.</h1>
			<form onSubmit={handleSubmit}>
				<label
					htmlFor="search"
					className="block mb-4">
					Look for images...
				</label>
				<input
					ref={searchRef}
					className="block w-full mb-14 text-slate-800 py-3 px-2 text-md outline-gray-500 rounded border border-slate-400"
					type="text"
					placeholder="Look for something..."
				/>
			</form>
			{/**
			 * Afficharge d'erreur
			 */}
			{photoApiData.error.state && <p>{photoApiData.error.msg}</p>}
			{/**Pas d'erreur et pas de resultat */}
			{!photoApiData.error.state && photoApiData.photos.length === 0 && !photoApiData.loading && <p>No images available for this query</p>}
			<ul className="grid grid-cols-[repeat(auto-fill,minmax(250px,1fr))] auto-rows-[175px] gap-4 justify-center ">
				{!photoApiData.loading &&
					photoApiData.photos.length !== 0 &&
					photoApiData.photos.map((photo, index) => {
						if (photoApiData.photos.length === index + 1) {
							return (
								<li
									key={photo.id}
									ref={lastPicRef}>
									<img
										className="w-full h-full object-cover"
										src={photo.urls.regular}
										alt={photo.alt_description}
									/>
								</li>
							);
						} else {
							return (
								<li key={photo.id}>
									<img
										className="w-full h-full object-cover"
										src={photo.urls.regular}
										alt={photo.alt_description}
									/>
								</li>
							);
						}
					})}
			</ul>
			{/**Loader */}
			{photoApiData.loading && !photoApiData.error.state && (
				<img
					src={spinner}
					className="block mx-auto"
				/>
			)}
			{/** Quand on atteind la derniere page */}
			{photoApiData.maxPages === pagneNumber && <p className="mt-10">No more images to show for that query</p>}
		</>
	);
}
